import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {artistPublish, deleteArtist, fetchGetArtists} from "../../store/actions/mainActions";
import {Button, Card, CardBody, CardText, CardTitle} from "reactstrap";
import ImageThumbnail from "../ImageThumbnail/ImageThumbnail";
import {NavLink} from "react-router-dom";

import './Artists.css';

class Artists extends Component {
    componentDidMount() {
        this.props.loadArtist();
    };

    deleteHandler = (artistId) => {
        this.props.deleteArtist(artistId);
        this.props.history.push('/');
    };

    publishHandler = (artistId) => {
        this.props.artistPublish(artistId)
    };

    render() {
        let artist = this.props.artists.map(id => {
            return (
                <Card key={id._id} className="artist-card">
                    <CardBody>
                        <CardTitle>{id.name}</CardTitle>
                    </CardBody>
                    <ImageThumbnail image={id.photo}/>
                    <CardBody>
                        <CardText>{id.info}</CardText>
                        {
                            id.published === true ?
                                <Fragment>
                                    <NavLink to={`/album/${id._id}/${id.name}`}>Albums</NavLink>
                                    {this.props.user && this.props.user.role === 'admin' ? <Button color="danger" className="delete" onClick={() => this.deleteHandler(id._id)}>Delete</Button> : null}
                                </Fragment> :
                                    this.props.user && this.props.user.role === 'admin' ?
                                            <Fragment>
                                                <p className="not-publish">Not published</p>
                                                <Button color="danger" className="delete" onClick={() => this.deleteHandler(id._id)}>Delete</Button>
                                                <Button onClick={() => this.publishHandler(id._id)} className="publish">Publish</Button>
                                            </Fragment> :
                                                <Fragment>
                                                    <p className="not-publish">Not published</p>
                                                </Fragment>
                        }
                    </CardBody>
                </Card>
            )
        });
        return (
            <div>
                <h4>Artists list</h4>
                <div className="artist-list">
                    {artist}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    artists: state.albumsArtists.artists,
});

const mapDispatchToProps = dispatch => ({
    loadArtist: () => dispatch(fetchGetArtists()),
    deleteArtist: (artistId) => dispatch(deleteArtist(artistId)),
    artistPublish: (artistId) => dispatch(artistPublish(artistId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Artists);
