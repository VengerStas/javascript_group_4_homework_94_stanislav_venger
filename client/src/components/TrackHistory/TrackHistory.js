import React, {Component} from 'react';
import {fetchGetTrackHistory} from "../../store/actions/mainActions";
import {connect} from "react-redux";

import './TrackHistory.css';
import {Redirect} from "react-router-dom";

class TrackHistory extends Component {
    componentDidMount() {
        if (!this.props.user) return <Redirect to="/login" />;
        this.props.fetchGetTrackHistory();
    }

    render() {
        if (!this.props.user) return <Redirect to="/login" />;
        return (
            this.props.trackHistory ? this.props.trackHistory.map((track) => {
                return (
                    <div key={track._id}>
                        <div className="track-item">
                            <p><strong>{track.track.album.artist.name}</strong></p>
                            <p>{track.track.name}</p>
                            <p>{track.dateTime}</p>
                        </div>
                    </div>
                )
            }) : null
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    trackHistory: state.albumsArtists.trackHistory,
});

const mapDispatchToProps = dispatch => ({
    fetchGetTrackHistory: () => dispatch(fetchGetTrackHistory())
});

export default connect(mapStateToProps, mapDispatchToProps)(TrackHistory);
