import React, {Component, Fragment} from 'react';
import {albumPublish, deleteAlbum, fetchGetAlbum} from "../../store/actions/mainActions";
import {connect} from "react-redux";
import {Button, Card, CardBody, CardText, CardTitle} from "reactstrap";
import AlbumThumbnail from "../AlbumThumbnail/AlbumThumbnail";
import {NavLink} from "react-router-dom";

import './Albums.css';

class Albums extends Component {
    componentDidMount() {
        this.props.loadAlbum(this.props.match.params.id);
    }

    deleteHandler = (albumId) => {
        this.props.deleteAlbum(albumId);
    };

    publishHandler = (albumId) => {
        this.props.albumPublish(albumId)
    };

    render() {
        let album = this.props.albums.map(id => {
            return (
                <Card key={id._id} className="album-card">
                    <CardBody>
                        <CardTitle className="album-name">{this.props.match.params.artist + '/' + id.name}</CardTitle>
                    </CardBody>
                    <AlbumThumbnail image={id.photo}/>
                    <CardBody>
                        <CardText>{id.info}</CardText>
                        <CardText>{id.date}</CardText>
                        {
                            id.published === true ?
                                <Fragment>
                                    <NavLink to={`/track/${id._id}/${this.props.match.params.artist}/${id.name}`}>Tracks</NavLink>
                                    {this.props.user && this.props.user.role === 'admin' ? <Button color="danger" className="delete" onClick={() => this.deleteHandler(id)}>Delete</Button> : null}
                                </Fragment> :
                                    this.props.user && this.props.user.role === 'admin' ?
                                        <Fragment>
                                            <p className="not-publish">Not published</p>
                                            <Button color="danger" className="delete" onClick={() => this.deleteHandler(id)}>Delete</Button>
                                            <Button onClick={() => this.publishHandler(id)} className="publish">Publish</Button>
                                        </Fragment> :
                                            <Fragment>
                                                <p className="not-publish">Not published</p>
                                            </Fragment>
                        }
                    </CardBody>
                </Card>
            )
        });
        return (
            <div>
                <NavLink to="/">Artist list</NavLink>
                <h4>Album list</h4>
                <div>
                    {album}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    albums: state.albumsArtists.albums,
});

const mapDispatchToProps = dispatch => ({
    loadAlbum: id => dispatch(fetchGetAlbum(id)),
    deleteAlbum: albumId => dispatch(deleteAlbum(albumId)),
    albumPublish: albumId => dispatch(albumPublish(albumId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Albums);
