import React, {Component, Fragment} from 'react';
import {Button, Col, Container, Form, FormGroup, Input, Label} from "reactstrap";
import FormElement from "../UI/Form/FormElement";
import {fetchGetAlbum} from "../../store/actions/mainActions";
import {connect} from "react-redux";

class TrackForm extends Component {
    state = {
      artist: '',
      album: '',
      name: '',
      number: '',
      link: '',
      time: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.onSubmit({...this.state});
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    render() {
        return (
            <Fragment>
                <Container>
                    <Form onSubmit={this.submitFormHandler}>
                        <FormGroup row>
                            <Label sm={2} for="artist">Artist</Label>
                            <Col sm={10}>
                                <Input
                                    type="select" required
                                    name="artist" id="artist"
                                    value={this.state.artist}
                                    onChange={this.inputChangeHandler}
                                    onClick={() => this.props.fetchGetAlbum(this.state.artist)}
                                >
                                    <option value="" disabled>Please select artist...</option>
                                    {this.props.artists.map(artist => (
                                        <option key={artist._id} value={artist._id}>{artist.name}</option>
                                    ))}
                                </Input>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={2} for="album">Album</Label>
                            <Col sm={10}>
                                <Input
                                    type="select" required
                                    name="album" id="album"
                                    value={this.state.album}
                                    onChange={this.inputChangeHandler}
                                >
                                    <option value="" disabled>Please select album...</option>
                                    {this.props.albums.map(album => (
                                        <option key={album._id} value={album._id}>{album.name}</option>
                                    ))}
                                </Input>
                            </Col>
                        </FormGroup>
                        <FormElement propertyName="name" title="Track title" type="text" value={this.state.name} onChange={this.inputChangeHandler}/>
                        <FormElement propertyName="number" title="Track number" type="text" value={this.state.number} onChange={this.inputChangeHandler}/>
                        <FormElement propertyName="link" title="Link for video" type="text" value={this.state.link} onChange={this.inputChangeHandler}/>
                        <FormElement propertyName="time" title="Duration" type="text" value={this.state.time} onChange={this.inputChangeHandler}/>
                        <FormGroup row>
                            <Col sm={{offset:2, size: 10}}>
                                <Button type="submit" color="primary">Add</Button>
                            </Col>
                        </FormGroup>
                    </Form>
                </Container>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    albums: state.albumsArtists.albums,
});

const mapDispatchToProps = dispatch => ({
    fetchGetAlbum: (id) => dispatch(fetchGetAlbum(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(TrackForm);