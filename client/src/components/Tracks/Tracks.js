import React, {Component, Fragment} from 'react';
import {addTrackToHistory, deleteTrack, fetchGetTracks, trackPublish} from "../../store/actions/mainActions";
import {connect} from "react-redux";
import {NavLink} from "react-router-dom";

import './Tracks.css';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

class Tracks extends Component {
    state = {
        video: null
    };

    componentDidMount() {
        this.props.loadTracks(this.props.match.params.id);
    }

    showModal = (id) => {
      this.setState({video: id})
    };

    hideModal = () => {
        this.setState({video: null})
    };

    deleteHandler = (trackId) => {
        this.props.deleteTrack(trackId);
    };

    publishHandler = (trackId) => {
        this.props.trackPublish(trackId)
    };

    render() {
        if (!this.props.tracks) return <div>Loading</div>;
        let track = this.props.tracks.map(id => {
            return (
                <div key={id._id}>
                    <div style={{cursor: "pointer"}} className="track-item">
                        <p>{id.number}</p>
                        <p>{id.name}</p>
                        <p>{id.time}</p>
                        {
                            id.published === true ?
                                <Fragment>
                                    {
                                       this.props.user && id.link ? <Button className="show-video" onClick={() => this.showModal(id)}>Show video</Button> : null
                                    }
                                    {this.props.user ? <Button onClick={() => this.props.postTrack({track: id._id})}>Add track</Button> : null}
                                    {this.props.user && this.props.user.role === 'admin' ? <Button color="danger" className="delete" onClick={() => this.deleteHandler(id)}>Delete</Button> : null}
                                </Fragment> :
                                    this.props.user && this.props.user.role === 'admin' ?
                                        <Fragment>
                                            <p className="not-publish">Not published</p>
                                            <Button color="danger" className="delete" onClick={() => this.deleteHandler(id)}>Delete</Button>
                                            <Button onClick={() => this.publishHandler(id)} className="publish">Publish</Button>
                                        </Fragment> :
                                            <Fragment>
                                                <p className="not-publish">Not published</p>
                                            </Fragment>
                        }
                    </div>
                </div>
            )
        });
        return (
            <div>
                <NavLink to="/">Artist list</NavLink>
                <h4 className="tracks-title">Track list</h4>
                <div className="track list">
                    <p className="track-album-name">Album name: <span className="album-title">{this.props.match.params.artist}/{this.props.match.params.album}</span></p>
                    {track}
                </div>
                <Modal isOpen={!!this.state.video} toggle={this.hideModal}>
                    {
                        this.state.video && (
                            <Fragment>
                                <ModalHeader toggle={this.hideModal}>{this.state.video.name}</ModalHeader>
                                <ModalBody>
                                    <iframe title={this.state.video.name} width="560" height="315" src={this.state.video.link}
                                            frameBorder="0"
                                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                            allowFullScreen/>
                                </ModalBody>
                                <ModalFooter>
                                    <Button color="secondary" onClick={this.hideModal}>Cancel</Button>
                                </ModalFooter>
                            </Fragment>
                        )
                    }
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    tracks: state.albumsArtists.tracks,
});

const mapDispatchToProps = dispatch => ({
    loadTracks: id => dispatch(fetchGetTracks(id)),
    postTrack: track => dispatch(addTrackToHistory(track)),
    deleteTrack: track => dispatch(deleteTrack(track)),
    trackPublish: track => dispatch(trackPublish(track))
});

export default connect(mapStateToProps, mapDispatchToProps)(Tracks);