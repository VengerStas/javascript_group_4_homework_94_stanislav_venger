import React, {Component, Fragment} from 'react';
import {Button, Col, Container, Form, FormGroup, Input, Label} from "reactstrap";
import FormElement from "../UI/Form/FormElement";

class AlbumForm extends Component {
    state = {
        artist: '',
        name: '',
        date: '',
        photo: null
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData, this.state);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
            <Fragment>
                <Container>
                    <Form onSubmit={this.submitFormHandler}>
                        <FormGroup row>
                            <Label sm={2} for="artist">Artist</Label>
                            <Col sm={10}>
                                <Input
                                    type="select" required
                                    name="artist" id="artist"
                                    value={this.state.artist}
                                    onChange={this.inputChangeHandler}
                                >
                                    <option value="" disabled>Please select artist...</option>
                                    {this.props.artists.map(artist => (
                                        <option key={artist._id} value={artist._id}>{artist.name}</option>
                                    ))}
                                </Input>
                            </Col>
                        </FormGroup>
                        <FormElement propertyName="name" title="Album title" type="text" value={this.state.name} onChange={this.inputChangeHandler}/>
                        <FormElement propertyName="date" title="Album date" type="text" value={this.state.date} onChange={this.inputChangeHandler}/>
                        <FormGroup row>
                            <Label sm={2} for="image">Album photo</Label>
                            <Col sm={10}>
                                <Input
                                    type="file"
                                    name="photo" id="image"
                                    onChange={this.fileChangeHandler}
                                />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col sm={{offset:2, size: 10}}>
                                <Button type="submit" color="primary">Add</Button>
                            </Col>
                        </FormGroup>
                    </Form>
                </Container>
            </Fragment>
        );
    }
}

export default AlbumForm;