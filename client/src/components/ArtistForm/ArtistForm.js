import React, {Component, Fragment} from 'react';
import {Button, Col, Container, Form, FormGroup, Input, Label} from "reactstrap";
import FormElement from "../UI/Form/FormElement";

class ArtistForm extends Component {
    state = {
       name: '',
       info: '',
       photo: null
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };


    render() {
        return (
            <Fragment>
                <Container>
                    <Form onSubmit={this.submitFormHandler}>
                        <FormElement propertyName="name" title="Artist name" type="text" value={this.state.name} onChange={this.inputChangeHandler}/>
                        <FormElement propertyName="info" title="Artist info" type="text" value={this.state.info} onChange={this.inputChangeHandler}/>
                        <FormGroup row>
                            <Label sm={2} for="image">Artist photo</Label>
                            <Col sm={10}>
                                <Input
                                    type="file"
                                    name="photo" id="image"
                                    onChange={this.fileChangeHandler}
                                />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col sm={{offset:2, size: 10}}>
                                <Button type="submit" color="primary">Add</Button>
                            </Col>
                        </FormGroup>
                    </Form>
                </Container>
            </Fragment>
        );
    }
}

export default ArtistForm;