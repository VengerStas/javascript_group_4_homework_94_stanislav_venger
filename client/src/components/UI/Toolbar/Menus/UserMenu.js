import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, NavLink, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const UserMenu = ({user, logout}) => (
    <UncontrolledDropdown nav inNavbar>
        <DropdownToggle nav caret>
            Hello, {user.displayName}
            <img className="user-photo" src={user.photo} alt="user avatar"/>
        </DropdownToggle>
        <DropdownMenu right>
            <DropdownItem>
                <NavLink tag={RouterNavLink} to="/track_history" exact>Track History</NavLink>
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem>
                <NavLink tag={RouterNavLink} to="/artist/new" exact>Add artist</NavLink>
            </DropdownItem>
            <DropdownItem>
                <NavLink tag={RouterNavLink} to="/album/new" exact>Add album</NavLink>
            </DropdownItem>
            <DropdownItem>
                <NavLink tag={RouterNavLink} to="/track/new" exact>Add track</NavLink>
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem onClick={logout}>
                Log out
            </DropdownItem>
        </DropdownMenu>
    </UncontrolledDropdown>
);

export default UserMenu;