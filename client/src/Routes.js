import React from 'react';
import {Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Artists from "./components/Artists/Artists";
import Albums from "./components/Albums/Albums";
import Tracks from "./components/Tracks/Tracks";
import TrackHistory from "./components/TrackHistory/TrackHistory";
import AddArtist from "./containers/AddArtist/AddArtist";
import AddAlbum from "./containers/AddAlbum/AddAlbum";
import AddTrack from "./containers/AddTrack/AddTrack";

// const ProtectedRoute = ({isAllowed, ...props}) => {
//     return isAllowed ? <Route {...props} /> : <Redirect to="/login"/>
// };

const Routes = () => {
    return (
        <Switch>
            <Route path="/" exact component={Artists}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/album/:id/:artist" exact component={Albums}/>
            <Route path='/track/:id/:artist/:album' exact component={Tracks}/>
            <Route path='/artist/new' exact component={AddArtist}/>
            <Route path='/album/new' exact component={AddAlbum}/>
            <Route path='/track/new' exact component={AddTrack}/>
            <Route path='/track_history' exact component={TrackHistory}/>
        </Switch>
    );
};

export default Routes;