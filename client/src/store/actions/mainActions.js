import axios from "../../axiosBase"
import {push} from "connected-react-router";
import {NotificationManager} from "react-notifications";

export const ARTISTS_REQUEST = "ARTISTS_REQUEST";
export const ARTISTS_REQUEST_SUCCESS = "ARTISTS_REQUEST_SUCCESS";
export const ALBUM_REQUEST = "ALBUM_REQUEST";
export const ALBUM_REQUEST_SUCCESS = "ALBUM_REQUEST_SUCCESS";
export const TRACK_REQUEST = "TRACK_REQUEST";
export const TRACK_REQUEST_SUCCESS = "TRACK_REQUEST_SUCCESS";
export const TRACK_HISTORY_SUCCESS = "TRACK_HISTORY_SUCCESS";
export const TRACK_HISTORY_REQUEST = "TRACK_HISTORY_REQUEST";



export const artistsRequest = () => {
    return {type: ARTISTS_REQUEST}
};

export const artistRequestSuccess = artists => {
    return {type: ARTISTS_REQUEST_SUCCESS, artists}
};

export const albumsRequest = () => {
    return {type: ALBUM_REQUEST}
};

export const albumRequestSuccess = albums => {
    return {type: ALBUM_REQUEST_SUCCESS, albums}
};

export const trackRequest = () => {
    return {type: TRACK_REQUEST}
};

export const trackRequestSuccess = tracks => {
    return {type: TRACK_REQUEST_SUCCESS, tracks}
};

export const trackHistorySuccess = tracks => {
    return {type: TRACK_HISTORY_SUCCESS, tracks}
};

export const trackHistoryRequest = (tracks) => {
    return {type: TRACK_HISTORY_REQUEST, tracks}
};

export const fetchGetArtists = () => {
    return (dispatch) => {
        dispatch(artistsRequest());
        axios.get('/artist').then(response => {
            dispatch(artistRequestSuccess(response.data));
        })
    }
};

export const addArtist = artistData => {
  return dispatch => {
      return axios.post('/artist', artistData).then(
          () => {
              dispatch(artistsRequest());
              dispatch(push('/'));
          }
      )
  }
};

export const deleteArtist = artistId => {
    return (dispatch) => {
        return axios.delete('/artist/' + artistId).then(() => {
            NotificationManager.success('Artist has been deleted');
            dispatch(fetchGetArtists());
            dispatch(push('/'));
        })
    }
};

export const artistPublish = artistId => {
    return (dispatch) => {
        return axios.post(`/artist/${artistId}/publish`).then(() => {
            NotificationManager.success('Artist has been published');
            dispatch(fetchGetArtists());
        });
    }
};

export const fetchGetAlbum = id => {
    return (dispatch) => {
        dispatch(albumsRequest());
        axios.get('/album?artist=' + id).then(response => {
            dispatch(albumRequestSuccess(response.data))
        })
    }
};

export const addAlbum = (albumData, state) => {
    return (dispatch) => {
        return axios.post('/album', albumData).then(
            (result) => {
                dispatch(albumsRequest());
                dispatch(push(`/album/${state.artist}/${result.data.artist.name}`));

            }
        )
    }
};

export const deleteAlbum = album => {
    return (dispatch) => {
        const albumId = album._id;
        return axios.delete('/album/' + albumId).then(() => {
            NotificationManager.success('Album has been deleted');
            dispatch(fetchGetAlbum(album.artist._id));
        })
    }
};

export const albumPublish = album => {
    return (dispatch) => {
        const albumId = album._id;
        return axios.post(`/album/${albumId}/publish`).then(() => {
            NotificationManager.success('Album has been published');
            dispatch(fetchGetAlbum(album.artist._id));
        });
    }
};

export const fetchGetTracks = id => {
    return (dispatch) => {
        axios.get('/track?album=' + id).then(response => {
            dispatch(trackRequestSuccess(response.data))
        })
    }
};

export const addNewTrack = (trackData) => {
    return (dispatch) => {
        return axios.post('/track', trackData).then(
            (result) => {
                console.log(result.data);
                dispatch(trackRequest());
                dispatch(push(`/track/${result.data.album._id}/${result.data.album.artist.name}/${result.data.album.name}`));
            }
        )
    }
};

export const deleteTrack = track => {
    return (dispatch) => {
        const trackId = track._id;
        return axios.delete('/track/' + trackId).then(() => {
            NotificationManager.success('Track has been deleted');
            dispatch(fetchGetTracks(track.album._id));
        })
    }
};

export const trackPublish = track => {
    return (dispatch) => {
        const trackId = track._id;
        return axios.post(`/track/${trackId}/publish`).then(() => {
            NotificationManager.success('Track has been published');
            dispatch(fetchGetTracks(track.album._id));
        });
    }
};

export const addTrackToHistory = track => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        axios.post('/track_history', track, {headers: {'Authorization': token}}).then(() => {
            dispatch(trackHistorySuccess());
        })
    }
};

export const fetchGetTrackHistory = () => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        axios.get('/track_history', {headers: {'Authorization': token}}).then(response => {
            dispatch(trackHistoryRequest(response.data))
        })
    }
};
