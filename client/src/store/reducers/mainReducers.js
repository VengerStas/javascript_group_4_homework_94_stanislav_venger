import {
    ALBUM_REQUEST_SUCCESS,
    ARTISTS_REQUEST_SUCCESS,
    TRACK_HISTORY_REQUEST,
    TRACK_REQUEST_SUCCESS
} from "../actions/mainActions";

const initialState = {
    artists: [],
    albums: [],
    tracks: [],
    trackHistory: null
};

const mainReducers = (state = initialState, action) => {
    switch (action.type){
        case ARTISTS_REQUEST_SUCCESS:
            return {
                ...state,
                artists: action.artists
            };
        case ALBUM_REQUEST_SUCCESS:
            return {
                ...state,
                albums: action.albums
            };
        case TRACK_REQUEST_SUCCESS:
            return {
                ...state,
                tracks: action.tracks
            };
        case TRACK_HISTORY_REQUEST:
            return {
                ...state,
                trackHistory: action.tracks
            };
        default:
            return state;
    }
};


export default mainReducers;