import React, {Component} from 'react';
import {addAlbum, fetchGetArtists} from "../../store/actions/mainActions";
import {Redirect} from "react-router-dom";
import {connect} from "react-redux";
import AlbumForm from "../../components/AlbumForm/AlbumForm";

class AddAlbum extends Component {
    componentDidMount() {
        this.props.fetchArtist();
        if (!this.props.user) return <Redirect to="/login" />;
    }

    createAlbum = (albumData, state) => {
        this.props.addAlbum(albumData, state);
    };

    render() {
        if (!this.props.user) return <Redirect to="/login" />;
        return (
            <div className="album-form-block">
                <h4>Add album</h4>
                <AlbumForm
                    onSubmit={this.createAlbum}
                    artists={this.props.artists}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    artists: state.albumsArtists.artists
});

const mapDispatchToProps = dispatch => ({
    addAlbum: (albumData, state) => dispatch(addAlbum(albumData, state)),
    fetchArtist: () => dispatch(fetchGetArtists())
});

export default connect(mapStateToProps, mapDispatchToProps)(AddAlbum);