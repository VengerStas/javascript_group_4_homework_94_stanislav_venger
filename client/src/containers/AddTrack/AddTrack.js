import React, {Component} from 'react';
import {addNewTrack, fetchGetArtists} from "../../store/actions/mainActions";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import TrackForm from "../../components/TrackForm/TrackForm";

class AddTrack extends Component {
    componentDidMount() {
        this.props.fetchArtist();
        if (!this.props.user) return <Redirect to="/login" />;
    }

    createTrack = (trackData, state) => {
        this.props.addNewTrack(trackData, state);
    };


    render() {
        if (!this.props.user) return <Redirect to="/login" />;
        return (
            <div className="track-form-block">
                <h4>Add track</h4>
                <TrackForm
                    onSubmit={this.createTrack}
                    artists={this.props.artists}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    artists: state.albumsArtists.artists,
});

const mapDispatchToProps = dispatch => ({
    addNewTrack: (trackData, state) => dispatch(addNewTrack(trackData, state)),
    fetchArtist: () => dispatch(fetchGetArtists()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddTrack);