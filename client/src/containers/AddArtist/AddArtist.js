import React, {Component} from 'react';
import {connect} from "react-redux";
import ArtistForm from "../../components/ArtistForm/ArtistForm";
import {addArtist} from "../../store/actions/mainActions";
import {Redirect} from "react-router-dom";

class AddArtist extends Component {
    createArtist = artistData => {
        this.props.addArtist(artistData);
    };


    render() {
        if (!this.props.user) return <Redirect to="/login" />;
        return (
            <div className="add-artist-form">
                <h4>Add artist</h4>
                <ArtistForm
                    onSubmit={this.createArtist}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    addArtist: artistData => dispatch(addArtist(artistData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddArtist);