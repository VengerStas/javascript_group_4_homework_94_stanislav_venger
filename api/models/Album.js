const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AlbumSchema = new Schema ({
    name: {
        type: String,
        required: true
    },
    photo: String,
    date: {
        type: String,
        required: true
    },
    artist: {
        type: Schema.Types.ObjectId,
        ref: 'Artist',
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    published: {
        type: Boolean,
        required: true,
        default: false
    }
});

const Album = mongoose.model('Album', AlbumSchema);

module.exports = Album;