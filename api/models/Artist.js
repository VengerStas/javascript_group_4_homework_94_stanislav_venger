const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ArtistSchema = new Schema ({
   name: {
        type: String,
        required: true
   },
    photo: String,
    info: {
        type: String,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    published: {
        type: Boolean,
        required: true,
        default: false
    }
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;