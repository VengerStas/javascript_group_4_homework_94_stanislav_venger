const express = require('express');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const tryAuth = require('../middleware/tryAuth');
const Track = require('../models/Track');

const router = express.Router();

router.get('/', tryAuth, (req, res) => {
    let criteria = {published: true, album: req.query.album};

    if (req.user) {
        criteria  = {
            album: req.query.album,
            $or: [
                {published: true},
                {user: req.user},
            ]
        }
    }
    if (req.user && req.user.role === 'admin') {
        criteria  = {
            album: req.query.album,
            $or: [
                {published: true},
                {published: false},
                {user: req.user}
            ]
        }
    }

    Track.find(criteria).sort({number: 1}).populate('album', 'name')
        .then(track => res.send(track))
        .catch(() => res.sendStatus(500));
});

router.post('/', [auth, permit("admin", "user")], (req, res) => {
    const trackData = {...req.body, user: req.user._id};

    const track = new Track(trackData);

    track.save()
        .then(results => Track.populate(results, {path: 'album', populate: {path: 'artist'}})).then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

router.post('/:id/publish', [auth, permit("admin")], async (req, res) => {
    const track = await Track.findById(req.params.id);

    if (!track) {
        return res.sendStatus(404);
    }

    track.published = !track.published;

    await track.save();
    return res.send(track);
});

router.delete('/:id', [auth, permit("admin")], (req, res) => {
    Track.deleteOne({_id: req.params.id})
        .then(result => res.send(result))
        .catch(error => res.status(403).send(error))
});

module.exports = router;