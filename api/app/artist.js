const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const tryAuth = require('../middleware/tryAuth');


const Artist = require('../models/Artist');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPathArtist);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer ({storage});

const router = express.Router();

router.get('/', tryAuth, (req, res) => {
    let criteria = {published: true};

    if (req.user) {
        criteria = {
            $or: [
                {published: true},
                {user: req.user._id}
            ]
        }
    }

    if (req.user && req.user.role === 'admin') {
        criteria = {
            $or: [
                {published: false},
                {published: true},
                {user: req.user._id}
            ]
        }
    }

    Artist.find(criteria)
        .then(artist => res.send(artist))
        .catch(() => res.sendStatus(500));
});

router.post('/', [auth, permit("admin", "user")], upload.single('photo'), (req, res) => {
    const artistData = {...req.body, user: req.user._id};

    if (req.file) {
        artistData.photo = req.file.filename;
    }

    const artist = new Artist(artistData);

    artist.save()
        .then(results => res.send(results))
        .catch(error => res.status(400).send(error));
});

router.post('/:id/publish', [auth, permit("admin")], async (req, res) => {
    const artist = await Artist.findById(req.params.id);

    if (!artist) {
        return res.sendStatus(404);
    }

    artist.published = !artist.published;

    await artist.save();
    return res.send(artist);
});

router.delete('/:id', [auth, permit("admin")], (req, res) => {
    Artist.deleteOne({_id: req.params.id})
        .then(result => res.send(result))
        .catch(error => res.status(403).send(error))
});

module.exports = router;