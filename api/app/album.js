const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const tryAuth = require('../middleware/tryAuth');

const Album = require('../models/Album');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPathAlbum);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer ({storage});

const router = express.Router();

router.get('/', tryAuth, (req, res) => {
    let criteria = {published: true, artist: req.query.artist};

    if (req.user) {
        criteria  = {
            artist: req.query.artist,
            $or: [
                {published: true},
                {user: req.user}
            ]
        }
    }
    if (req.user && req.user.role === 'admin') {
        criteria  = {
            artist: req.query.artist,
            $or: [
                {published: true},
                {published: false},
                {user: req.user}
            ]
        }
    }
    Album.find(criteria).sort({date: 1}).populate('artist', 'name')
        .then(artist => res.send(artist))
        .catch(() => res.sendStatus(500))
});

router.get('/:id', (req, res) => {
    Album.findById(req.param.id)
        .then(album => {
            if (album) res.send(album);
            else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});

router.post('/', [auth, permit("admin", "user")], upload.single('photo'), (req, res) => {
    const albumData = {...req.body, user: req.user._id};

    if (req.file) {
        albumData.photo = req.file.filename;
    }

    const album = new Album(albumData);

    album.save()
        .then(results => Album.populate(results, 'artist').then(result => res.send(result)))
        .catch(error => res.status(400).send(error));
});

router.post('/:id/publish', [auth, permit("admin")], async (req, res) => {
    const album = await Album.findById(req.params.id);

    if (!album) {
        return res.sendStatus(404);
    }

    album.published = !album.published;

    await album.save();
    return res.send(album);
});

router.delete('/:id', [auth, permit("admin")], (req, res) => {
    Album.deleteOne({_id: req.params.id})
        .then(result => res.send(result))
        .catch(error => res.status(403).send(error))
});

module.exports = router;