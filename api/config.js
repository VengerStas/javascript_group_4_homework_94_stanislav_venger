const path = require('path');

const rootPath = __dirname;


module.exports = {
    rootPath,
    uploadPathArtist: path.join(rootPath, 'public/uploads/artist'),
    uploadPathAlbum: path.join(rootPath, 'public/uploads/album'),
    dbUrl: 'mongodb://localhost/radio',
    mongoOption: { useNewUrlParser: true, useCreateIndex: true},
    facebook: {
        appId: '828486540842286',
        appSecret: 'e4097ad518749f3973853f7d1f5b9a33'
    }
};