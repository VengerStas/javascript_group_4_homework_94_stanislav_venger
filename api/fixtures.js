const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');

const User = require('./models/Users');
const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOption);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const users =  await User.create ({
        username: 'user',
        password: '123',
        displayName: 'User name',
        role: 'user',
        token: nanoid()
    }, {
        username: 'admin',
        password: '123',
        displayName: 'Admin',
        role: 'admin',
        token: nanoid()
    });

    const [oxxxymiron, guf, basta] = await Artist.create (
        {user: users[0]._id, name: 'Oxxxymiron', photo: 'oxxxymiron.jpg', info: 'рэп-, грайм-, фристайл-исполнитель', published: true},
        {user: users[0]._id, name: 'GUF', photo: 'guf.jpg', info: 'российский рэп-исполнитель, сооснователь и участник группы CENTR', published: true},
        {user: users[0]._id, name: 'Basta', photo: 'basta.jpg', info: 'российский музыкант, телерадиоведущий, актёр, сценарист, режиссёр и продюсер.', published: true}
    );

    const [gorgorod, bastaGuf, basta4, nintendo] = await Album.create (
        {user: users[0]._id, name: 'Горгород', photo: 'gorgorod.jpg', date: '13.11.2015', artist: oxxxymiron._id, published: true},
        {user: users[0]._id, name: 'Баста Гуф', photo: 'bastaguf.jpg', date: '10.11.2010', artist: guf._id, published: true},
        {user: users[0]._id, name: 'Баста 4', photo: 'chk.jpg', date: '20.04.2013', artist: basta._id, published: true},
        {user: users[0]._id, name: 'Nintendo', photo: 'hqdefault.jpg', date: '03.10.2011', artist: basta._id, published: true}
    );

    await Track.create(
        {
            user: users[0]._id,
            name: 'Переплетено',
            number: 5,
            time: '4:51',
            album: gorgorod._id,
            published: true
        },
        {
            user: users[0]._id,
            name: 'Слова мэра',
            number: 9,
            link: 'https://www.youtube.com/embed/cVyYHC4QzCo',
            time: '4:01',
            album: gorgorod._id,
            published: true
        },
        {
            user: users[0]._id,
            name: 'Заколоченное',
            number: 8,
            time: '4:09',
            album: bastaGuf._id,
            published: true
        },
        {
            user: users[0]._id,
            name: 'Чайный пьяница',
            number: 12,
            link: 'https://www.youtube.com/embed/cRm54SJxcao',
            time: '5:15',
            album: bastaGuf._id,
            published: true
        },
        {
            user: users[0]._id,
            name: 'Чистый кайф',
            number: 14,
            time: '6:05',
            album: basta4._id,
            published: true
        },
        {
            user: users[0]._id,
            name: 'Пуэрчик покрепче',
            number: 10,
            link: 'https://www.youtube.com/embed/HJy9MQ3bUZ4',
            time: '4:54',
            album: basta4._id,
            published: true
        },
        {
            user: users[0]._id,
            name: 'Черный пистолет',
            number: 11,
            time: '4:04',
            album: nintendo._id,
            published: true
        },
        {
            user: users[0]._id,
            name: 'Ран Вася Ран',
            number: 12,
            link: 'https://www.youtube.com/embed/_4uKo-HWZ7Y',
            time: '3:18',
            album: nintendo._id,
            published: true
        },
        {
            user: users[0]._id,
            name: 'Криминал',
            number: 14,
            time: '3:54',
            album: nintendo._id,
            published: true
        }
    );

    return connection.close();
};

run().catch(error => {
    console.log('Oops something happened...', error);
});